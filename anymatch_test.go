package anymatch;

import (
    "testing"
);

//String -> Input
//Bool -> Expected output
type TST struct{
    string;
    bool;
}

func TestMatch(t *testing.T){
    patterns:=[]string{"file[1-3].test", "ignoreme*", "/nothing/*"};
    files:=[]TST{
        {"file1.test", true},
        {"file4.test", false},
        {"dontignoreme", false},
        {"ignoremenow", true},
        {"/nothing/something", true},
    };
    Init(patterns);
    for _,v:=range(files){
        m,err:=Match(v.string);
        if err != nil {
            t.Errorf("Match of %s errored: %s\n", v.string, err);
        } else if m != v.bool {
            t.Errorf("Unexpected %t on %s. Expecting %t\n", m, v.string, v.bool);
        }else{
            t.Logf("Tested ok file %s. Got %t\n", v.string, m);
        }
    }
}
