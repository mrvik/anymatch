//Match any of the elements of an array using filepath.Match
//Init with a filename to read, a byte array or a slice
//
//You can call the Init functions on any moment to refresh the match slice
package anymatch;

import (
    "errors"
    "fmt"
    "io/ioutil"
    "strings"
    "path/filepath"
);

var matchSlice *[]string;

//Init anymatch with a file path to read ignores from
func InitFile(file string) error{
    contents,err:=ioutil.ReadFile(file);
    if err != nil {
        return err;
    }
    matchSlice=GenLinesSlice(&contents);
    return err; //err should be nil if everything is OK
}

//Init anymatch with a pointer to
func Init(ignores []string){
    matchSlice=&ignores;
}

func GenLinesSlice(btext *[]byte) *[]string{
    text:=string(*btext);
    spl:=strings.Split(text, "\n");
    return &spl;
}

func Match(name string) (bool, error){
    if len(*matchSlice) < 1 {
        return false,nil; //Can't match anything on an empty slice. Not enough to raise an error
    }
    for _,v:=range(*matchSlice){
        if ok,err:=filepath.Match(v, name); ok||err!=nil{
            return ok,err;
        }
    }
    return false,nil;
}

func MatchIndex(s string, index int) (bool,error){
    if len(*matchSlice) <= index { //Last index of a 5 element slice is 4 so can't access len(slice)
        return false, errors.New(fmt.Sprintf("Invalid index %d", index));
    }
    return filepath.Match((*matchSlice)[index], s);
}
